# DESCRIPTION #

Mini-Pocopay is a simple application that makes payments between 2 accounts.

### Starting the application? ###

```
#!gradle

gradle jettyRun
```


### Guidelines ###

URL | Method | Parameters | Description
--- | ------ | ---------- | -----------
/pocopay/account | POST | username, amount(optional, default: 100) | Create a new account
/pocopay/account/{username} | GET | - | Request account state
/pocopay/payment | POST | senderUsername, receiverUsername, amount | Make a payment from one account to another
/pocopay/payment/{username} | GET | - | Request account payments


### Test ###

[Postman](http://www.getpostman.com/): https://www.getpostman.com/collections/84bec7ec5567e3000c5f