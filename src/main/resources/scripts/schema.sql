CREATE TABLE account (
    id INTEGER IDENTITY PRIMARY KEY,
    username VARCHAR(32) NOT NULL,
    amount DECIMAL(10,2) NOT NULL,
    UNIQUE(username)
);

CREATE TABLE payment (
    id INTEGER IDENTITY PRIMARY KEY,
    sender_account_id INTEGER NOT NULL,
    receiver_account_id INTEGER NOT NULL,
    amount DECIMAL(10,2) NOT NULL,
    date TIMESTAMP NOT NULL,
    CONSTRAINT fk_payment_account_sender FOREIGN KEY (sender_account_id) REFERENCES account(id),
    CONSTRAINT fk_payment_account_receiver FOREIGN KEY (receiver_account_id) REFERENCES account(id)
);
