package ee.korogui.exception;

public class PocopayException extends RuntimeException {

    public PocopayException(String message) {
        super(message);
    }
    
}
