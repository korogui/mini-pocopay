package ee.korogui.exception.model;

import java.util.ArrayList;
import java.util.List;

public class FormError {

    private String message = "Error!";
    private final List<FieldErrorView> fieldErrors = new ArrayList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<FieldErrorView> getFields() {
        return fieldErrors;
    }

    public void addFieldError(String field, String error) {
        this.fieldErrors.add(new FieldErrorView(field, error));
    }

    public void addFieldErrors(List<FieldErrorView> errors) {
        this.fieldErrors.addAll(errors);
    }
}
