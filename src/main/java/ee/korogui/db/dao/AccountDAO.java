package ee.korogui.db.dao;

import ee.korogui.db.model.Account;

public interface AccountDAO {
    
    void insertAccount(Account account);
    
    Account getByUsername(String username);
    
    Account getById(String username);
    
    void updateAmount(Account account);
    
}
