package ee.korogui.db.dao;

import ee.korogui.db.model.Payment;
import java.util.List;

public interface PaymentDAO {

    Payment getById(Integer id);

    List<Payment> findPaymentsFromAccount(Integer senderAccountId);

    void insertPayment(Payment transaction);
}
