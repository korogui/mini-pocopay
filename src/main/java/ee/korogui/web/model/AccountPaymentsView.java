package ee.korogui.web.model;

import ee.korogui.db.model.Account;
import ee.korogui.db.model.Payment;
import java.util.ArrayList;
import java.util.List;

public class AccountPaymentsView {

    private Account account;
    private List<Payment> payments = new ArrayList<>();

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

}
