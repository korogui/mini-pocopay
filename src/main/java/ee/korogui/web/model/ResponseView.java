package ee.korogui.web.model;

public class ResponseView {

    private boolean success;
    private Object result;

    public ResponseView() {
    }

    public ResponseView(Object result) {
        this.success = true;
        this.result = result;
    }

    public ResponseView(boolean success, Object result) {
        this.success = success;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
    
    public void setError(Object result) {
        this.success = false;
        this.result = result;
    }
}
