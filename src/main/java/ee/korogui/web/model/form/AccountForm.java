package ee.korogui.web.model.form;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;

public class AccountForm {

    @NotNull
    private String username;
    private BigDecimal amount = BigDecimal.valueOf(100);

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
