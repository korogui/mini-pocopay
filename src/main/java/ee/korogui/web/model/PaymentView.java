package ee.korogui.web.model;

import ee.korogui.db.model.Account;
import ee.korogui.db.model.Payment;

public class PaymentView {
    private Account account;
    private Payment payment;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}
