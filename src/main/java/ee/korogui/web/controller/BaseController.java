package ee.korogui.web.controller;

import ee.korogui.web.model.ResponseView;
import ee.korogui.exception.PocopayException;
import ee.korogui.exception.model.FieldErrorView;
import ee.korogui.exception.model.FormError;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;

public abstract class BaseController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(Exception.class)
    public ResponseView handleAllException(Exception ex) {
        logger.error(null, ex);

        ResponseView result = new ResponseView();
        result.setError(ex.getMessage());
        return result;
    }

    @ExceptionHandler(PocopayException.class)
    public ResponseView handlePocopayException(PocopayException ex) {
        logger.error(null, ex);

        ResponseView result = new ResponseView();
        result.setError(ex.getMessage());
        return result;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseView handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        logger.error(null, ex);

        FormError form = new FormError();

        final BindingResult bindingResult = ex.getBindingResult();
        final String globalErrorMessage = getGlobalErrorMessage(bindingResult);
        if (!StringUtils.isEmpty(globalErrorMessage)) {
            form.setMessage(globalErrorMessage);
        }
        form.addFieldErrors(getFieldErrorViews(bindingResult));

        return new ResponseView(false, form);
    }

    private List<FieldErrorView> getFieldErrorViews(final BindingResult bindingResult) {
        List<FieldErrorView> result = new ArrayList<>();
        for (FieldError error : bindingResult.getFieldErrors()) {
            result.add(new FieldErrorView(error.getField(), error.getDefaultMessage()));
        }
        return result;
    }

    private String getGlobalErrorMessage(final BindingResult bindingResult) {
        StringBuilder builder = new StringBuilder();
        String separator = "";
        for (ObjectError error : bindingResult.getGlobalErrors()) {
            builder.append(separator.concat(error.toString()));
            separator = "\r\n";
        }
        return builder.toString();
    }

    ResponseView createResponse(Object result) {
        return new ResponseView(result);
    }

    ResponseView createErrorResponse(Object result) {
        return new ResponseView(false, result);
    }
}
