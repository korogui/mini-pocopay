package ee.korogui.web.controller;

import ee.korogui.web.model.form.PaymentForm;
import ee.korogui.web.model.ResponseView;
import ee.korogui.service.PaymentService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("payment")
public class PaymentController extends BaseController {

    @Autowired
    private PaymentService transactionService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public ResponseView makePayment(
        @RequestBody @Valid PaymentForm income) {

        return createResponse(transactionService.requestPayment(income));
    }

    @ResponseBody
    @RequestMapping(value = "{fromUsername}", method = RequestMethod.GET)
    public ResponseView findPayments(
        @PathVariable String fromUsername) {

        return createResponse(transactionService.findPayments(fromUsername));
    }
}
