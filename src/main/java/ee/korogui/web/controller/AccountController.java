package ee.korogui.web.controller;

import ee.korogui.db.model.Account;
import ee.korogui.web.model.form.AccountForm;
import ee.korogui.web.model.ResponseView;
import ee.korogui.service.AccountService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("account")
public class AccountController extends BaseController{

    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseView createAccount(@RequestBody @Valid AccountForm form) {

        Account account = accountService.create(form);
        
        return createResponse(account);
    }

    @RequestMapping(value="{username}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseView getAccount(@PathVariable String username) {

        Account account = accountService.getByUsername(username);
        
        return createResponse(account);
    }
    
    
}
