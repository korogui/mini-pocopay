package ee.korogui.service;

import ee.korogui.db.dao.PaymentDAO;
import ee.korogui.db.model.Account;
import ee.korogui.db.model.Payment;
import ee.korogui.exception.PocopayException;
import ee.korogui.web.model.form.PaymentForm;
import ee.korogui.web.model.AccountPaymentsView;
import ee.korogui.web.model.PaymentView;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class PaymentService {

    @Autowired
    PaymentDAO paymentDAO;

    @Autowired
    AccountService accountService;

    @Transactional(readOnly = false)
    public PaymentView requestPayment(PaymentForm form) {
        PaymentView result = new PaymentView();

        BigDecimal amount = form.getAmount();
        Account sender = accountService.subtractAccountAmount(form.getSenderUsername(), amount);
        Account receiver = accountService.addAccountAmount(form.getReceiverUsername(), amount);
        Payment payment = insertPayment(sender.getId(), receiver.getId(), amount);

        result.setAccount(sender);
        result.setPayment(payment);

        return result;
    }

    @Transactional(readOnly = false)
    private Payment insertPayment(Integer senderAccountId, Integer receiverAccountId, BigDecimal amount) {
        Payment payment = new Payment(senderAccountId, receiverAccountId, amount);
        paymentDAO.insertPayment(payment);
        return payment;
    }

    public AccountPaymentsView findPayments(String fromUsername) {
        AccountPaymentsView result = new AccountPaymentsView();
        Account account = getAccount(fromUsername);

        result.setAccount(account);
        result.setPayments(paymentDAO.findPaymentsFromAccount(account.getId()));

        return result;
    }

    private Account getAccount(String username) {
        Account result = accountService.getByUsername(username);
        if (result == null) {
            throw new PocopayException("Account '" + username + "' not found!");
        }

        return result;
    }

}
