package ee.korogui.service;

import ee.korogui.web.model.form.AccountForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ee.korogui.db.dao.AccountDAO;
import ee.korogui.db.model.Account;
import ee.korogui.exception.PocopayException;
import java.math.BigDecimal;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class AccountService {

    @Autowired
    private AccountDAO accountDAO;

    @Transactional(readOnly = false)
    public Account create(AccountForm form) {
        validate(form);

        Account account = createAccountInstance(form);
        accountDAO.insertAccount(account);
        return account;
    }

    private Account createAccountInstance(AccountForm form) {
        Account account = new Account();
        account.setUsername(form.getUsername());
        account.setAmount(form.getAmount());
        return account;
    }

    AccountForm getUser(String username) {
        AccountForm result = new AccountForm();
        result.setUsername(username);
        return result;
    }

    public Account getByUsername(String username) {
        Account result = accountDAO.getByUsername(username);
        if (result == null) {
            throw new PocopayException("Account '" + username + "' not found!");
        }
        return result;
    }

    private void validate(AccountForm form) {
        final String username = form.getUsername();
        Account existingAccount = accountDAO.getByUsername(username);
        if (existingAccount != null) {
            throw new PocopayException("Username '" + username + "' already exists!");
        }
    }

    @Transactional(readOnly = false)
    public Account subtractAccountAmount(String username, BigDecimal amount) {
        Account result = getByUsername(username);
        result.setAmount(result.getAmount().subtract(amount));
        if (result.getAmount().doubleValue() < 0) {
            throw new PocopayException("Not enough amount for the payment!");
        }
        accountDAO.updateAmount(result);
        return result;
    }

    @Transactional(readOnly = false)
    public Account addAccountAmount(String username, BigDecimal amount) {
        Account result = getByUsername(username);
        result.setAmount(result.getAmount().add(amount));
        accountDAO.updateAmount(result);
        return result;
    }

}
